var UnFriendFB = function() {
  var accesstoken = document.getElementById('fbatoken').value;
  // => https://stackoverflow.com/a/21367406/4723940
  $.ajax({
      url: "https://graph.facebook.com/me/friends?access_token=" + accesstoken,
      success: function(data) {
        jQuery.each(data.data, function() {
            $.ajax({
                url: "https://m.facebook.com/a/removefriend.php",
                data: "friend_id=" + this.id + "&fb_dtsg=AQC4AoV0&unref=profile_gear&confirm=Confirmer",
                async: true,
                type: "post"
              })
        });
      },
      dataType: "json"
  });
  // <= https://stackoverflow.com/a/21367406/4723940
};
